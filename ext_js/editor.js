
var fxmessage = function (e) {

  //console.log(e.origin); 
  console.log(e.data.fx_params);
  console.log(e);
  console.log(e.data.fx);
  console.log("postmessageclient");

  if(typeof editorFunctions[e.data.fx] !== 'undefined')
  {
    editorFunctions[e.data.fx](e.data.fx_params)

  window.parent.postMessage({
    'origin': e.origin
  }, "*");
}
}

window.addEventListener('message', fxmessage, false);

var editorFunctions =
{

  createButton: function (fx_params) { createButton(fx_params, $("body").find("._block")) },
  doRender: function (fx_params) {
   
    $.ajax({
      url: window.appUrl+'/api/renderengine?host=' + fx_params.host,
      type: 'POST',
      data: { jsonPage: fx_params.jsonPage },
     
      success: function (result) {
        
        var htmlnew = result;
        window.removeEventListener('message', fxmessage)
        $("body").html(htmlnew);
        createButton(fx_params, $("body").find("._block"))
      }
    });
  },
  addBlockMode(fx_params) {

    createAddBlockButton(fx_params, $("body").find("._block"))

  }

};





function createButton(fx_params, els) {
 
  els.each(function (a, b) {

    var elid = $(b).attr("id");
    var html = '<div style="background:#ccc;padding:7px;text-align:center;position:relative;z-index:255" class="actionrows" id="action_' + elid + '"><b>' + $(b).data("tp") + '</b>';

    if ($.inArray("edit", fx_params.typeBtn) > -1) {
      html += ' <button class="btn btn-xs btn-warning editblock" data-elid="' + elid + '"><i class="fas fa-pencil-alt" aria-hidden="true"></i></button>';
    }
    // if ($.inArray("delete", fx_params.typeBtn) > -1) {
    //   html += ' <button class="btn btn-xs btn-danger deleteblock" data-elid="' + elid + '"><i class="fas fa-trash-alt" aria-hidden="true"></i></button>';
    // }
    if ($.inArray("delete", fx_params.typeBtn) > -1) {
       html += ' <button class="btn btn-xs btn-danger" data-elid="' + elid + '" disabled><i class="fas fa-trash-alt" aria-hidden="true"></i></button>';
    }
    if ($.inArray("move", fx_params.typeBtn) > -1) {
      html += ' <button class="btn btn-xs btn-default moveblock" data-elid="' + elid + '"><i class="fa fa-arrows" aria-hidden="true"></i></button>';
    }

    html += '</div>';

    $(b).prepend(html);
    $(b).css("border", "1px dotted #777")
  })
  applyEdit($("body").find(".editblock"))
  applyDelete($("body").find(".deleteblock"))

}

//crea button aggiungi blocco e  init handler associati
function createAddBlockButton(fx_params, els) {
  $(".actionrows").remove()
  if(!els.length)
  {
    $("#sortable-block").append(addBlockBtnHtml("s_0_0", fx_params.blockCode));
    
    
  }
  else { 
    els.each(function (a, b) {
      var elid = $(b).attr("id");
      var splitted = elid.split("_");
      if (a) {
        elid = "s_" + splitted[1] + "_" + (parseInt(splitted[2]) + 1)
        $(b).append(addBlockBtnHtml(elid, fx_params.blockCode));
      }
      else {

        $(b).prepend(addBlockBtnHtml(elid, fx_params.blockCode));
        elid = "s_" + splitted[1] + "_" + (parseInt(splitted[2]) + 1)
        $(b).append(addBlockBtnHtml(elid, fx_params.blockCode));
      }


      $(b).css("border", "1px dotted #777")
    })
  }

  applyAddBlock($("body").find(".addblock"), fx_params.blockCode)
}

//make html button add block 
function addBlockBtnHtml(elid, code) {
  var html = '<div style="background:green;padding:7px;color:#fff;text-align:center"  data-elid="' + elid + '" data-code="' + code + '" class="addblock" id="action_' + elid + '">Aggiungi qui <i class="fa fa-plus" aria-hidden="true"></i>';
  html += '</div>';
  return html;
}


/* FX INIT HANDLER ACTION */

function applyEdit(els) {
  
  
  els.on("click", function () {
    var eldata = $(this).data()
    window.parent.postMessage({
      'fx': "itemClicked",
      'fx_params': { element: $("#" + eldata["elid"]).html(), selector: eldata["elid"] },
    }, "*");

  });
}

function applyAddBlock(els, blockCode) {
 
  els.on("click", function () {
    var eldata = $(this).data()
    window.parent.postMessage({
      'fx': "addBlocktoJson",
      'fx_params': { element: $("#" + eldata["elid"]).html(), selector: eldata["elid"], code: blockCode },
    }, "*");

  });
}
function applyDelete(els)
{
  els.on("click", function () {
    var eldata = $(this).data()
    window.parent.postMessage({
      'fx': "itemDeleted",
      'fx_params': { element: $("#" + eldata["elid"]).html(), selector: eldata["elid"] },
    }, "*");
  });
  
}


/*  FX SORTABLE PER TRASCINAMENTO E ORDINE BLOCCHI */ 
var el = document.getElementById('sortable-block');
if(typeof Sortable !== "undefined")
{
Sortable.create(el, {
  sort: true,
  animation: 100,
  onEnd: function (evt) {
    var itemIdPrev = $(evt.item).prev().attr("id");
    var itemIdSelected = $(evt.item).attr("id");

    window.parent.postMessage({
      'fx': "itemMoved",
      'fx_params': { elid: itemIdSelected, prevelid: itemIdPrev,oldIndex:evt.oldIndex,newIndex:evt.newIndex },
    }, "*");

  },
});
}


window.parent.postMessage({
  'fx': "itemLoaded",
  'fx_params': { element:true },
}, "*");