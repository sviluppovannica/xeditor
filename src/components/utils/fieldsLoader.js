const forEach = require('lodash').forEach

const fieldComponents = {}

const coreFields = require.context('../formfields', false, /^\.\/field([\w-_]+)\.vue$/)

forEach(coreFields.keys(), (key) => {
  const compName = key.replace(/^\.\//, '').replace(/\.vue/, '')
  fieldComponents[compName] = coreFields(key).default
})

export default fieldComponents
