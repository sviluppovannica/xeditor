export default {
  methods: {

    formatDateIta (date, anno, nomegiorno) {
      if (typeof anno === 'undefined') { anno = false }
      if (typeof nomegiorno === 'undefined') { nomegiorno = false }

      var days = ['Domenica', 'Lunedi', 'Martedi', 'Mercoledi', 'Giovedi', 'Venerdi', 'Sabato']

      // data odierna
      var today = new Date()
      var gg, mm, aaaa
      var datalabel
      var time = null
      // var dayName = days[today.getDay()];

      gg = today.getDate()
      mm = today.getMonth() + 1
      aaaa = today.getFullYear()
      if (mm < 10) mm = '0' + mm
      if (gg < 10) gg = '0' + gg
      if (date.length > 10) {
        time = date.split(' ')[1]
        date = date.split(' ')[0]
      }
      var datearr = date.split('-')
      if (datearr[2] == gg && datearr[1] == mm && aaaa == datearr[0]) {
        datalabel = 'Oggi'
      } else {
        datalabel = datearr[2] + '/' + datearr[1]
        if (anno) {
          datalabel += '/' + datearr[0]
        }
        if (nomegiorno) {
          if (datearr.length) {
            var day = new Date(date)

            datalabel = days[day.getDay()] + ' ' + datalabel
          }
        }
      }
      if (time) {
        datalabel = datalabel + ' ' + time
      }

      return datalabel
    },

    getTodayDate () {
      var today = new Date()
      var gg, mm, aaaa
      gg = today.getDate()
      mm = today.getMonth() + 1
      aaaa = today.getFullYear()
      if (mm < 10) mm = '0' + mm
      if (gg < 10) gg = '0' + gg
      return aaaa + '-' + mm + '-' + gg
    },
    chunk (array,itemsPerRow) {
      return Array.from(Array(Math.ceil(array.length / itemsPerRow)).keys())
    },
  }
}
